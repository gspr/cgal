// Copyright (c) 2009-2024
// Utrecht University (The Netherlands),
// ETH Zurich (Switzerland),
// INRIA Sophia-Antipolis (France),
// Max-Planck-Institute Saarbruecken (Germany),
// and Tel-Aviv University (Israel).  All rights reserved.
//
// This file is part of CGAL (www.cgal.org)
//
// $URL: https://github.com/CGAL/cgal/blob/v6.0-beta1/Installation/include/CGAL/version.h $
// $Id: include/CGAL/version.h 9075f96 $
// SPDX-License-Identifier: LGPL-3.0-or-later OR LicenseRef-Commercial
//
// Author(s)     : -

#ifndef CGAL_VERSION_H
#define CGAL_VERSION_H

#ifndef SWIG
#define CGAL_VERSION 6.0-beta1
#define CGAL_GIT_HASH 9075f96529a3ffe42718f0d23a7cfe1482a892f9
#endif
#define CGAL_VERSION_NR 1060000910
#define CGAL_SVN_REVISION 99999
#define CGAL_RELEASE_DATE 20240616

#include <CGAL/version_macros.h>

#endif
